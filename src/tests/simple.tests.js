import Basepage from "../po/pages/base.page.js";
const basepage = new Basepage();

describe('Pastebin page', () => {

    beforeEach(async () => {
        await basepage.open();
    })


    it('Automate script for creating new paste', async () => {

        await $(".css-47sehv").click(); //clicking the Agree button on the "We value your privacy" modal

        await basepage.form.input("code").setValue("Hello from WebDriver");
        await basepage.form.input("expiration").click();
        await basepage.form.expirationValue("tenMinutes").waitForDisplayed();
        await basepage.form.expirationValue("tenMinutes").click();
        await basepage.form.input("title").setValue("helloweb");
        await basepage.form.createPasteBtn.click();
    })
})