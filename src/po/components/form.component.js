export default class FormComponent {

    get rootEl() {
        return $(".post-form");
    }

    input(name) {
        const selectors = {
            code: "#postform-text",
            expiration: "#select2-postform-expiration-container",
            title: "#postform-name",
        }

        return this.rootEl.$(selectors[name.toLowerCase()])
    }

    get createPasteBtn() {
        return this.rootEl.$(".form-btn-container");
    }

    expirationValue(value) {
        const selectors = {
            tenMinutes: "//*[@id='select2-postform-expiration-results']/li[3]"
        }
        return this.rootEl.$(selectors[value]);
    }
}